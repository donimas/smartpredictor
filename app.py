import os.path
import sys
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import tensorflow.python.keras.callbacks
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dropout
import tensorflow as tf
import platform

if platform.system() == "Darwin":
    tf.config.threading.set_inter_op_parallelism_threads(4)
else:
    tf.config.list_physical_devices('GPU')

STOCK_INDEX = sys.argv[1]


def create_dataset(dataset, time_step=1):
    dataX, dataY = [], []
    for i in range(len(dataset) - time_step - 1):
        a = dataset[i:(i + time_step), 0]
        dataX.append(a)
        dataY.append(dataset[i + time_step, 0])
    return numpy.array(dataX), numpy.array(dataY)


dataset_train = pd.read_csv('./data/' + STOCK_INDEX + '.csv')
training_set = dataset_train.iloc[:, 1:2].values

dataset_train.head()

sc = MinMaxScaler(feature_range=(0, 1))
training_set_scaled = sc.fit_transform(training_set)
X_train = []
y_train = []
# print(range(60, len(dataset_train)))
# print(len(training_set_scaled))
# exit()
for i in range(60, len(dataset_train)):
    print(i)
    X_train.append(training_set_scaled[i - 60:i, 0])
    y_train.append(training_set_scaled[i, 0])
X_train, y_train = np.array(X_train), np.array(y_train)

X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))

model = Sequential()
# model = tensorflow.keras.models.load_model('train_model')
model.add(LSTM(units=50, return_sequences=True, input_shape=(X_train.shape[1], 1)))
model.add(Dropout(0.2))

model.add(LSTM(units=50, return_sequences=True))
model.add(Dropout(0.2))

model.add(LSTM(units=50, return_sequences=True))
model.add(Dropout(0.2))

model.add(LSTM(units=50))
model.add(Dropout(0.2))

model.add(Dense(units=1))
#
model.compile(optimizer='adam', loss='mean_squared_error')

# model.save("train_model")

checkpoint_path = "training_1/" + STOCK_INDEX + ".ckpt"

checkpoint_dir = os.path.dirname(checkpoint_path)

cp_callbacks = tensorflow.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path, save_weights_only=True, verbose=1)
#
# model.load_weights(checkpoint_path)
# model.save_weights(checkpoint_path.format(epoch=0))

model.fit(X_train, y_train, epochs=100, batch_size=64, callbacks=[cp_callbacks])

dataset_test = pd.read_csv('last/' + STOCK_INDEX + '.csv')
real_stock_price = dataset_test.iloc[:, 1:2].values
# print("REAL")
# print(real_stock_price)
dataset_total = pd.concat((dataset_train['Open'], dataset_test['Open']), axis=0)
inputs = dataset_total[len(dataset_total) - len(dataset_test) - 60:].values
inputs = inputs.reshape(-1, 1)
inputs = sc.transform(inputs)
X_test = []
for i in range(60, dataset_test.shape[0] + 60):
    X_test.append(inputs[i - 60:i, 0])
X_test = np.array(X_test)
X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))

training_size = int(len(inputs) * 0.25)
test_data = inputs[training_size:len(dataset_train), :1]

x_input = test_data[abs(60-len(test_data)):].reshape(1, -1)
x_input.shape
temp_input = list(x_input)
temp_input = temp_input[0].tolist()

lst_output = []
n_steps = x_input.shape[1]
i = 0
# print(temp_input)
# print(x_input)
while (i < 30):

    if (len(temp_input) > x_input.shape[1]):
        # print(temp_input)
        x_input = np.array(temp_input[1:])
        print("{} day input {}".format(i, x_input))
        x_input = x_input.reshape(1, -1)
        x_input = x_input.reshape((1, n_steps, 1))
        # print(x_input)
        yhat = model.predict(x_input, verbose=0)
        print("{} day output {}".format(i, yhat))
        temp_input.extend(yhat[0].tolist())
        temp_input = temp_input[1:]
        # print(temp_input)
        lst_output.extend(yhat.tolist())
        i = i + 1
    else:
        x_input = x_input.reshape((1, n_steps, 1))
        yhat = model.predict(x_input, verbose=0)
        print(yhat[0])
        temp_input.extend(yhat[0].tolist())
        print(len(temp_input))
        lst_output.extend(yhat.tolist())
        i = i + 1

print("LST OUTPUT")
print(lst_output)
predicted_stock_price = model.predict(X_test)
predicted_stock_price = sc.inverse_transform(predicted_stock_price)

print("REALSTOCKPRICE")
print(real_stock_price)
print(len(real_stock_price))
plt.plot(real_stock_price, color='black', label=STOCK_INDEX + ' Stock Price')
plt.plot(predicted_stock_price, color='green', label='Predicted ' + STOCK_INDEX + ' Stock Price')
day_pred = np.arange(len(real_stock_price), len(real_stock_price) + 30)
print("FUTUTE")
print(sc.inverse_transform(lst_output))
plt.plot(day_pred, sc.inverse_transform(lst_output), label="Future")
plt.title(STOCK_INDEX + ' Stock Price Prediction')
plt.xlabel('Time')
plt.ylabel(STOCK_INDEX + ' Stock Price')
plt.legend()
plt.show()
