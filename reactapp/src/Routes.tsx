import React from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';
import { RouteWithLayout } from './components';
import { default as MainLayout } from './layouts';
import { createBrowserHistory } from "history";

import {MainPage, ListPage, HistoryPage, IndexPage} from './views';

const history = createBrowserHistory({
  forceRefresh: true,
});


const Routes = () => {

  return (
    <Switch>
      <RouteWithLayout
              component={HistoryPage}
              path="/:index/prediction"
              layout={MainLayout}
            />

      <RouteWithLayout
              component={IndexPage}
              path="/:index"
              layout={MainLayout}
            />
      <RouteWithLayout
        component={ListPage}
        path="/"
        layout={MainLayout}
      />
      <Redirect to="/not-found" />
    </Switch>
  );
};

export default Routes;
