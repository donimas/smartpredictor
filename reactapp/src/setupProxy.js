const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/api',
    createProxyMiddleware({
      target: 'http://api:80',
      changeOrigin: true,
    })
  );
  app.use(
    '/docs',
    createProxyMiddleware({
      target: 'http://api:80',
      changeOrigin: true,
    })
  );
  app.use(
    '/openapi.json',
    createProxyMiddleware({
      target: 'http://api:80',
      changeOrigin: true,
    })
  );
};