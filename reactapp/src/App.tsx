import React from 'react';
import './App.css';
import {Router} from 'react-router-dom';
import 'antd/dist/antd.css';
import Routes from './Routes';
import { createBrowserHistory } from 'history';

// console.disableYellowBox = true;

/* eslint-disable */
const browserHistory = createBrowserHistory();

function App() {
  return (
    <Router history={browserHistory}>
      <Routes/>
    </Router>
  );
}

export default App;
