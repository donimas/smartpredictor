import React from 'react';


const Main = ({children}) => {
    return ( <div className="App">
    <header className="App-header">
        {children}
    </header>
    </div>)
}


export default Main;