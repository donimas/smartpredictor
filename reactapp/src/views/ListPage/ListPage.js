import React, { useState, useEffect} from 'react';
import { Table, Input } from 'antd';
import {SearchOutlined} from '@ant-design/icons';
import axios from 'axios';

const columns = [
  {
    title: 'Индекс',
    dataIndex: 'code',
    key: 'code',
    render: code => <a href={"/"+code}>{code}</a>,
  },
  {
    title: 'Имя',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Тренирован',
    dataIndex: 'is_trained',
    key: 'is_trained',
    render: isTrained => (isTrained) ? "Да" : "Нет",
  }
];

export default function ListPage(){

    const [data, setData] = useState([]);
    const [dataSource, setDataSource] = useState([]);
    const [trainStatus, setTrainStatus] = useState([]);

    useEffect(()=>{
      axios.get("/api/stock")
      .then(response=>{
        let stockList = response.data;
        axios.get("/api/stock/train_progress")
        .then(res=> {
          let status = res.data;
          setTrainStatus(status);
          for(let x=0; x<stockList.length; x++){
            // console.log(stockList[x]);
            let st = stockList[x];
            if(st !== undefined){
              stockList[x]['is_trained'] = status[st.code];
              console.log(stockList[x]);

            }
          }
          setData(stockList);
        });
      }).catch(error=>{console.log(error)})

    } ,[]);

    const onSearch = e => {
      axios.get("/api/stock?q="+ e.target.value).then(res => {
        setDataSource(res.data);
        setData(res.data);
      });
    }

    return (<>
    <div className="certain-category-search-wrapper" style={{ width: 450 }}>
      <br/>
      <Input
        className="certain-category-search"
        size="large"
        onChange={onSearch}
        placeholder="Поиск"
        suffix={<SearchOutlined type="search" className="certain-category-icon"/>}
     />
    </div>
    <br />
    <Table columns={columns} dataSource={data} loading={data.length===0} />
  </>);
};