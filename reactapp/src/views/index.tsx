export { default as MainPage } from './Main';
export { default as HistoryPage } from './History';
export { default as IndexPage } from './IndexPage';
export { default as ListPage } from './ListPage';