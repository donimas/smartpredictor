import React, { useState, useEffect } from 'react';

import { Line } from '@ant-design/charts';

const Charts = (index: string) => {
  const [data, setData] = useState([]);
  useEffect(() => {
    asyncFetch();
  }, []);
  const asyncFetch = () => {
    fetch('/api/predict/' + index)
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => {
        console.log('fetch data failed', error);
      });
  };
  var config = {
    data: data,
    // padding: 'auto',
    xField: 'Date',
    yField: 'scales',
    xAxis: { tickCount: 5 },
    smooth: true,
  };
  return <Line {...config} />;
};

export default Charts;