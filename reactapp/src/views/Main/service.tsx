import axios from 'axios';

export const predict = (index: string) => {
    return axios.post("/api/predict", {index: index});
}