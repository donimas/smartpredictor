import React, {useState} from 'react';
import {Button, DatePicker, Input, Steps} from 'antd';
import moment from 'moment';
import {predict} from './service';
const dateFormat = 'DD.MM.YYYY';

const { Step } = Steps;

function Main() {
    const [current, setCurrent] = useState(0);
    const [btnText, setBtnText] = useState("Прогноз");
    const [index, setIndex] = useState("TSLA");
    const [predictDate, setPredictDate] = useState(moment());

    const [status, setStatus] = useState(0);


    const onChange = (ee: number) => {
        console.log('onChange:', ee);
        setCurrent(ee);
    };

    const FirstStep = () => <Input value={index} onChange={(e) => {setIndex(e.target.value)}} />;

    const SecondStep = () => <DatePicker
    value={predictDate}
    onChange={(e) => {setPredictDate(moment(e, dateFormat))}}
    format={dateFormat} />;

    const callPredict = () => {
        setStatus(1);
        setBtnText("В процессе...");
        predict(index).then((res)=>{
            setBtnText("Прогноз");
            setStatus(0);
            console.log(res.data.content);
        }).catch((err)=>{
            console.error(err.message);
        });
    }


    const Overall = () => <div>
        <h1>{index}</h1>
        <h2>{predictDate.format(dateFormat)}</h2>
        <Button type="primary" size="large" loading={status === 1} onClick={callPredict} > {btnText} </Button>
    </div>;

      
    return (<p>
            <Steps direction="horizontal"  current={current} onChange={onChange}>
                <Step title="Выберите котировку" />
                <Step title="Укажите дату" />
                <Step title="Сделайте прогнозирование" />
            </Steps>
            <div>
                { current === 0 && FirstStep() }
                { current === 1 && SecondStep() }
                { current === 2 && Overall() }
            </div>
        </p>);
}

export default Main;