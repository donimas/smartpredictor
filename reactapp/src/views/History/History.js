import React, { useState, useEffect } from 'react';
import {Table, Typography} from 'antd';
import { Line } from '@ant-design/charts';
const {Title} = Typography;

const columns = [
  {
    title: 'Дата',
    dataIndex: 'Date',
    key: 'Date',
  },
  {
    title: 'Цена открытия',
    dataIndex: 'scales',
    key: 'scales',
  }
];

const DemoLine = (props) => {
  const index = props.match.params.index;
  const [data, setData] = useState([]);
  useEffect(() => {
    asyncFetch();
  }, []);
  const asyncFetch = () => {
    fetch('https://sp.sapasoft.kz/api/predict/' + index + '?date_from=2021-01-01&date_to=2021-08-23')
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => {
        console.log('fetch data failed', error);
      });
  };
  var config = {
    data: data,
    padding: 'auto',
    xField: 'Date',
    yField: 'scales',
    xAxis: { tickCount: 5 },
    smooth: true,
    width: 1080,
    loading: data.length===0
  };
  return (<>
  <a href={"/"+index}><Title level={3}>{index}</Title></a>
  <Line {...config} />
  <br/>
  <Table columns={columns} dataSource={data} loading={data.length===0}/>
  </>);
};

export default DemoLine;