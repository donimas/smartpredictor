import axios from 'axios';
import React from 'react';
import {Table, Typography} from 'antd';
import { Line, Stock } from '@ant-design/charts';


const { Title } = Typography;


export default function IndexPage(props){

    const index = props.match.params.index;

    const [history, setHistory] = React.useState([]);
    const [trainStatus, setTrainStatus] = React.useState([]);

    const columns = [
        {
          title: 'Date',
          dataIndex: 's_date',
          key: 's_date',
          sorter: (a, b) => new Date(a.s_date) >= new Date(b.s_date),
          sortDirections: ['descend', 'ascend'],
        },
        {
          title: 'Open',
          dataIndex: 'o_price',
          key: 'o_price',
        },
        {
          title: 'High',
          dataIndex: 'h_price',
          key: 'h_price',
        },
        {
          title: 'Low',
          dataIndex: 'l_price',
          key: 'l_price',
        },
        {
          title: 'Close',
          dataIndex: 'c_price',
          key: 'c_price',
        },
        {
          title: 'Adj Close',
          dataIndex: 'adj_close',
          key: 'adj_close',
        },
        {
          title: 'Volume',
          dataIndex: 'volume',
          key: 'volume',
        }
      ];

    React.useEffect(()=>{
        axios.get("/api/stock/"+index+"/history")
        .then(res => setHistory(res.data))
        .catch(error => console.log(error));
        axios.get("/api/stock/train_progress")
        .then(res=> {
          setTrainStatus(res.data);
        });
    },[]);

    var config = {
        data: history,
        // padding: 'auto',
        xField: 's_date',
        yField: ['o_price', 'c_price', 'h_price', 'l_price'],
        // xAxis: { tickCount: 5 },
        // smooth: true,
        width: 1080,
      };
    return (<>
    <Title level={3}>{index}</Title>
    {trainStatus[index] && <a href={"/"+index+"/prediction"}>Прогноз</a>}
    <Stock {...config} loading={history.length===0} />
    <br />
    <Table columns={columns} dataSource={history} loading={history.length===0}/>
    </>);
}