from keras.models import Sequential
import tensorflow as tf

(train_images, train_labels), (test_images, test_labels) = tf.keras.datasets.mnist.load_data()

train_labels = train_labels[:1000]
test_labels = test_labels[:1000]

train_images = train_images[:1000].reshape(-1, 28 * 28) / 255.0
test_images = test_images[:1000].reshape(-1, 28 * 28) / 255.0


def create_model():
  regressor = tf.keras.models.Sequential()

  regressor.add(LSTM(units=50, return_sequences=True, input_shape=(X_train.shape[1], 1)))
  regressor.add(Dropout(0.2))

  regressor.add(LSTM(units=50, return_sequences=True))
  regressor.add(Dropout(0.2))

  regressor.add(LSTM(units=50, return_sequences=True))
  regressor.add(Dropout(0.2))

  regressor.add(LSTM(units=50))
  regressor.add(Dropout(0.2))

  regressor.add(Dense(units=1))

  regressor.compile(optimizer='adam',
                loss=tf.losses.SparseCategoricalCrossentropy(from_logits=True),
                metrics=[tf.metrics.SparseCategoricalAccuracy()])

  return regressor
# Create a new model instance
model = create_model()

# Load the previously saved weights
model.load_weights("./training_1/cp.ckpt")

# Re-evaluate the model
loss, acc = model.evaluate(test_images, test_labels, verbose=2)
print("Restored model, accuracy: {:5.2f}%".format(100 * acc))