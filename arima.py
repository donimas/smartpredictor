import pandas as pd
from statsmodels.tsa.stattools import adfuller
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
import matplotlib.pyplot as plt
from statsmodels.tsa.arima_model import ARIMA, ARMA
import statsmodels.api as sm
import datetime
from dateutil.relativedelta import relativedelta

# plt.rcParams.update({'figure.figsize': (9, 7), 'figure.dpi': 120})
#
# # Import data
# df = pd.read_csv('sampledata.csv', names=['value'], header=0)
#
# # Original Series
# fig, axes = plt.subplots(3, 2, sharex=True)
# axes[0, 0].plot(df.value)
# axes[0, 0].set_title('Original Series')
# plot_acf(df.value, ax=axes[0, 1])
#
# # 1st Differencing
# axes[1, 0].plot(df.value.diff())
# axes[1, 0].set_title('1st Order Differencing')
# plot_acf(df.value.diff().dropna(), ax=axes[1, 1])
#
# # 2nd Differencing
# axes[2, 0].plot(df.value.diff().diff())
# axes[2, 0].set_title('2nd Order Differencing')
# plot_acf(df.value.diff().diff().dropna(), ax=axes[2, 1])
#
# plt.show()
#
# # 1,1,2 ARIMA Model
# model = ARIMA(df.value, order=(1, 1, 2))
# model_fit = model.fit(disp=0)
# print(model_fit.summary())
#
# # 1,1,1 ARIMA Model
# model = ARIMA(df.value, order=(1, 1, 1))
# model_fit = model.fit(disp=0)
# print(model_fit.summary())
#
# # Plot residual errors
# residuals = pd.DataFrame(model_fit.resid)
# fig, ax = plt.subplots(1, 2)
# residuals.plot(title="Residuals", ax=ax[0])
# residuals.plot(kind='kde', title='Density', ax=ax[1])
# plt.show()
#
# # Actual vs Fitted
# model_fit.plot_predict(dynamic=False)
# plt.show()


#### VERSION 2 PORTLAND

data = pd.read_csv('portland.csv')

data['Month'] = pd.to_datetime(data['Month'], errors='coerce', format='%Y-%m-%d')
data.dropna(subset=["Month"], inplace=True)
data.set_index('Month', inplace=True)

data.columns = ['price']
data['price'] = data['price'].astype(int)
# data.plot()

x = data['price']
result = adfuller(x)
# plot_acf(data['price first difference'].iloc[1:], lags=30)
# plot_pacf(data['price first difference'].iloc[1:], lags=30)
# Build Model
model = ARMA(data['price'], order=(2, 2))
model_fit = model.fit()

model = sm.tsa.statespace.SARIMAX(data['price'], order=(2, 1, 2), seasonal_order=(2, 1, 2, 6))
result = model.fit()

# data['forcast_SARIMA_1'] = result.predict(start=99, end=112, dynamic=True)
# data[['price', 'forcast_SARIMA_1']].plot(figsize=(20, 5))


start = datetime.datetime.strptime("2021-07-28", "%Y-%m-%d")

date_list = [start + relativedelta(days=x) for x in range(0, 30)]

future_prediction = pd.DataFrame(index=date_list, columns=data.columns)
data = pd.concat([data, future_prediction])
print(len(date_list))
print(len(data))
# exit()
dep = 31
step = 50 * 5 + 15
data['future_prediction'] = result.predict(start=304-31-step, end=304-step, dynamic=True)
data[['price', 'future_prediction']].plot(figsize=(10, 6))
plt.grid(True)
# model = ARIMA(df.value, order=(3, 2, 1))
# fitted = model.fit(disp=-1)
# print(fitted.summary())

# Forecast
# fc, se, conf = fitted.forecast(405, alpha=0.05)  # 95% conf
#
# # Make as pandas series
# fc_series = pd.Series(fc, index=15)
# lower_series = pd.Series(conf[:, 0], index=15)
# upper_series = pd.Series(conf[:, 1], index=15)
#
# # Plot
# plt.figure(figsize=(12, 5), dpi=100)
# plt.plot(df.value, label='training')
# plt.plot(df.value, label='actual')
# plt.plot(fc_series, label='forecast')
# plt.fill_between(lower_series.index, lower_series, upper_series,
#                  color='k', alpha=.15)
# plt.title('Forecast vs Actuals')
# plt.legend(loc='upper left', fontsize=8)
# plt.show()

plt.show()
