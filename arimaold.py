import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pylab import rcParams
from statsmodels.tsa.stattools import adfuller
from pandas.plotting import autocorrelation_plot
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
import statsmodels.api as sm
from statsmodels.tsa.arima_model import ARIMA, ARIMA_DEPRECATION_WARN
from pandas.tseries.offsets import DateOffset

import warnings

warnings.filterwarnings('ignore', 'statsmodels.tsa.arima_model.ARMA',
                        FutureWarning)
warnings.filterwarnings('ignore', 'statsmodels.tsa.arima_model.ARIMA',
                        FutureWarning)

warnings.warn(ARIMA_DEPRECATION_WARN, FutureWarning)

df = pd.read_csv('time_series_data.csv')
print(df.head())

# Updating the header
df.columns = ["Date", "Open"]
df.head()
df.describe()
df.set_index('Date', inplace=True)

rcParams['figure.figsize'] = 15, 7
df.plot()
test_result = adfuller(df['Open'])


def adfuller_test(sales):
    result = adfuller(sales)
    labels = ['ADF Test Statistic', 'p-value', '#Lags Used', 'Number of Observations']
    for value, label in zip(result, labels):
        print(label + ' : ' + str(value))
    if result[1] <= 0.05:
        print("strong evidence against the null hypothesis(Ho), reject the null hypothesis. Data is stationary")
    else:
        print("weak evidence against null hypothesis,indicating it is non-stationary ")


print(adfuller_test(df['Open']))

df['Sales First Difference'] = df['Open'] - df['Open'].shift(1)
df['Seasonal First Difference'] = df['Open'] - df['Open'].shift(12)
print(df.head())
print(adfuller_test(df['Seasonal First Difference'].dropna()))
df['Seasonal First Difference'].plot()

autocorrelation_plot(df['Open'])

fig = plt.figure(figsize=(12, 8))
ax1 = fig.add_subplot(211)
fig = sm.graphics.tsa.plot_acf(df['Seasonal First Difference'].dropna(), lags=20, ax=ax1)
ax2 = fig.add_subplot(212)
fig = sm.graphics.tsa.plot_pacf(df['Seasonal First Difference'].dropna(), lags=20, ax=ax2)

# For non-seasonal data
# p=1, d=1, q=0 or 1

model = ARIMA(df['Open'], order=(1, 1, 1))
model_fit = model.fit()
print(model_fit.summary())

df['forecast'] = model_fit.predict(start=73, end=103, dynamic=True)
df[['Open', 'forecast']].plot(figsize=(12, 8))

model = sm.tsa.statespace.SARIMAX(df['Open'], order=(1, 1, 1), seasonal_order=(1, 1, 1, 12))
results = model.fit()
df['forecast'] = results.predict(start=73, end=103, dynamic=True)
df[['Open', 'forecast']].plot(figsize=(12, 8))

print(df.index[-1])
print(DateOffset(months=0))
exit()

future_dates = [df.index[-1] + DateOffset(months=x) for x in range(0, 24)]
future_datest_df = pd.DataFrame(index=future_dates[1:], columns=df.columns)

future_datest_df.tail()

future_df = pd.concat([df, future_datest_df])

future_df['forecast'] = results.predict(start=104, end=120, dynamic=True)
future_df[['Open', 'forecast']].plot(figsize=(12, 8))
