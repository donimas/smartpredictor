from fastapi import FastAPI
from rest.routers import users, predict, stock

app = FastAPI()

app.include_router(users.router)
app.include_router(predict.router)
app.include_router(stock.router)
