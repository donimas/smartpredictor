from sqlalchemy.orm import Session
from rest.models.predict import Predict
from rest.schema.Predict import PredictBase
import logging

logger = logging.getLogger(__name__)

def get_predict(db: Session, user_id: int):
    return db.query(Predict).filter(Predict.id == user_id).first()

def create_predict(db: Session, data: PredictBase):
    record = Predict(
        request_id=data.request_id,
        ref_key=data.ref_key,
        jsondata=data.jsondata,
    )
    db.add(record)
    db.commit()
    db.refresh(record)
    return record