from sqlalchemy.orm import Session
from sqlalchemy import desc
from rest.models.history import History
from rest.schema.History import HistoryBase
from datetime import datetime
import logging

logger = logging.getLogger(__name__)

def get(db: Session, index: str, date: datetime):
    return db.query(History).filter(History.index == index, History.s_date == date).first()


def last(db: Session, index: str):
    return db.query(History).filter(History.index == index).order_by(desc(History.s_date)).first()

def get_by_index(db: Session, index: str):
    return db.query(History).filter(History.index == index).all()

def list(db: Session):
    return db.query(History).all()

def create(db: Session, data: HistoryBase):
    record = History(
        index=data.index,
        s_date=data.s_date,
        o_price=data.o_price,
        h_price=data.h_price,
        l_price=data.l_price,
        c_price=data.c_price,
        adj_close=data.adj_close,
        volume=data.volume,
    )
    db.add(record)
    db.commit()
    db.refresh(record)
    return record