from sqlalchemy.orm import Session
from rest.models.stock import Stock
from rest.schema.Stock import StockBase
import logging

logger = logging.getLogger(__name__)

def get(db: Session, code: str):
    return db.query(Stock).filter(Stock.code == code).first()

def list(db: Session, q: str = None):
    if q:
        return db.query(Stock).filter((Stock.code.ilike("%"+q+"%") | Stock.name.ilike("%"+q+"%"))).order_by(Stock.id).all()
    else:
        return db.query(Stock).order_by(Stock.id).all()

def create(db: Session, data: StockBase):
    record = Stock(
        code=data.code,
        name=data.name,
    )
    db.add(record)
    db.commit()
    db.refresh(record)
    return record