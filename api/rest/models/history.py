from sqlalchemy import Boolean, Column, Date, ForeignKey, Integer, BigInteger, String, Float
from sqlalchemy.orm import relationship
from rest.db.database import Base


class History(Base):
    __tablename__ = "history"
    id = Column(BigInteger, primary_key=True, index=True)
    index = Column(String, foreign_key=True, index=True)
    s_date = Column(Date, nullable=False, index=True)
    o_price = Column(Float, nullable=False)
    h_price = Column(Float, nullable=False)
    l_price = Column(Float, nullable=False)
    c_price = Column(Float, nullable=False)
    adj_close = Column(Float, nullable=False)
    volume = Column(BigInteger, nullable=False)