from sqlalchemy import Boolean, Column, Date, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from rest.db.database import Base


class Predict(Base):
    __tablename__ = "predict"
    id = Column(Integer, primary_key=True, index=True)
    request_id = Column(Integer)
    ref_key = Column(String, index=True)
    jsondata = Column(String)