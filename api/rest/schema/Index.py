from typing import List, Optional
from datetime import date, datetime, time, timedelta
from pydantic import BaseModel



class Index(BaseModel):
    index: str
    date_from: str
    date_to: str
