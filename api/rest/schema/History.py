from typing import List, Optional
from datetime import date, datetime, time, timedelta
from pydantic import BaseModel

class HistoryBase(BaseModel):
    index: Optional[str] = None
    s_date: Optional[datetime] = None
    o_price: Optional[float] = None
    h_price: Optional[float] = None
    l_price: Optional[float] = None
    c_price: Optional[float] = None
    adj_close: Optional[float] = None
    volume: Optional[int] = None

class HistorySchema(HistoryBase):
    id: int
