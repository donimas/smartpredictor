from typing import List, Optional
from datetime import date, datetime, time, timedelta
from pydantic import BaseModel

class PredictBase(BaseModel):
    request_id: Optional[int] = None
    ref_key: Optional[str] = None
    jsondata: Optional[str] = None

class PredictSchema(PredictBase):
    id: int
