from typing import List, Optional
from datetime import date, datetime, time, timedelta
from pydantic import BaseModel

class StockBase(BaseModel):
    code: Optional[str] = None
    name: Optional[str] = None

class StockSchema(StockBase):
    id: int
