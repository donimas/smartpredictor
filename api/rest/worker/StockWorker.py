import os.path
import yfinance as yf
from sqlalchemy.orm import Session
from datetime import datetime, timedelta
import logging
import json
from rest.crud import history
from rest.crud import stock
from rest.schema.History import HistoryBase
from rest.ml import TrainStock
logger = logging.getLogger(__name__)
from rest.utils.sandp500 import sandp500

def start(db: Session):
    for index in sandp500:
        st = stock.get(db, index)
        if st:
            last = history.last(db, index)
            start_date = last.s_date.strftime('%Y-%m-%d') if last else '2011-08-01'
            print(start_date)
            df = yf.download(index, start=start_date, end=datetime.today().strftime('%Y-%m-%d'))
            jsondf = df.to_json(orient = 'table')
            htable = json.loads(jsondf)
            # print(rec['data'][0])
            for data in htable['data']:
                schem = HistoryBase()
                schem.index=st.code
                schem.s_date = datetime.strptime(data['Date'],"%Y-%m-%dT%H:%M:%S.%fZ")
                schem.o_price=data['Open'],
                schem.c_price=data['Close'],
                schem.h_price=data['High'],
                schem.l_price=data['Low'],
                schem.adj_close=data['Adj Close'],
                schem.volume=data['Volume']
                if not history.get(db, schem.index, schem.s_date):
                    history.create(db, schem)

def start_train():
    for index in sandp500:
        if not os.path.isdir('./models/'+index):
            try:
                TrainStock(index, '2011-01-01', datetime.today().strftime('%Y-%m-%d'))
            except:
                continue
