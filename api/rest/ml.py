import os.path
import sys
import logging
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import tensorflow.python.keras.callbacks
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dropout
import tensorflow as tf
import platform
import yfinance as yf
from datetime import datetime, timedelta
import os
from rest.schema.Index import Index

logger = logging.getLogger(__name__)

os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

days_to_subtract = 100
load_start = datetime.today() - timedelta(days=days_to_subtract)
tenyear = datetime.today() - timedelta(days=10 * 365)

if platform.system() == "Darwin":
    tf.config.threading.set_inter_op_parallelism_threads(4)
else:
    tf.config.list_physical_devices('GPU')

def create_dataset(dataset, time_step=1):
    dataX, dataY = [], []
    for i in range(len(dataset) - time_step - 1):
        a = dataset[i:(i + time_step), 0]
        dataX.append(a)
        dataY.append(dataset[i + time_step, 0])
    return numpy.array(dataX), numpy.array(dataY)

def TrainStock( STOCK_INDEX: str, date_from: str, date_to: str ):
    end_date = datetime.today().strftime('%Y-%m-%d') if not date_to else date_to
    data_df = yf.download(STOCK_INDEX, start=date_from, end=end_date)
    data_df.to_csv('/app/data/' + STOCK_INDEX + '.csv')
    ldata_df = yf.download(STOCK_INDEX, start=date_from,
                          end=datetime.today().strftime('%Y-%m-%d'))
    ldata_df.to_csv('/app/last/' + STOCK_INDEX + '.csv')
    dataset_train = pd.read_csv('/app/data/' + STOCK_INDEX + '.csv')
    training_set = dataset_train.iloc[:, 1:2].values
    dataset_train.head()
    sc = MinMaxScaler(feature_range=(0, 1))
    training_set_scaled = sc.fit_transform(training_set)
    X_train = []
    y_train = []
    for i in range(60, len(dataset_train)):
        print(i)
        X_train.append(training_set_scaled[i - 60:i, 0])
        y_train.append(training_set_scaled[i, 0])
    X_train, y_train = np.array(X_train), np.array(y_train)
    X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
    model = Sequential()
    model.add(LSTM(units=50, return_sequences=True, input_shape=(X_train.shape[1], 1)))
    model.add(Dropout(0.2))
    model.add(LSTM(units=50, return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(units=50, return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(units=50))
    model.add(Dropout(0.2))
    model.add(Dense(units=1))
    model.compile(optimizer='adam', loss='mean_squared_error')
    epoch = 0
    checkpoint_path = "checkpoints/" + STOCK_INDEX + ".ckpt"
    checkpoint_dir = os.path.dirname(checkpoint_path)
    cp_callbacks = tensorflow.keras.callbacks.ModelCheckpoint(
        filepath=checkpoint_path,
        save_weights_only=True,
        verbose=1
    )
    model.fit(X_train, y_train, epochs=100, batch_size=64, callbacks=[cp_callbacks])
    dataset_test = pd.read_csv('/app/last/' + STOCK_INDEX + '.csv')
    model.save("models/"+STOCK_INDEX)
    real_stock_price = dataset_test.iloc[:, 1:2].values
    dataset_total = pd.concat((dataset_train['Open'], dataset_test['Open']), axis=0)
    inputs = dataset_total[len(dataset_total) - len(dataset_test) - 60:].values
    inputs = inputs.reshape(-1, 1)
    inputs = sc.transform(inputs)
    X_test = []
    for i in range(60, dataset_test.shape[0] + 60):
        X_test.append(inputs[i - 60:i, 0])
    X_test = np.array(X_test)
    X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
    training_size = int(len(inputs) * 0.25)
    test_data = inputs[training_size:len(dataset_train), :1]
    x_input = test_data[abs(60-len(test_data)):].reshape(1, -1)
    x_input.shape
    temp_input = list(x_input)
    temp_input = temp_input[0].tolist()
    lst_output = []
    n_steps = x_input.shape[1]
    i = 0
    while (i < 30):
        if (len(temp_input) > x_input.shape[1]):
            x_input = np.array(temp_input[1:])
            logger.warn("{} day input {}".format(i, x_input))
            x_input = x_input.reshape(1, -1)
            x_input = x_input.reshape((1, n_steps, 1))
            # print(x_input)
            yhat = model.predict(x_input, verbose=0)
            logger.warn("{} day output {}".format(i, yhat))
            temp_input.extend(yhat[0].tolist())
            temp_input = temp_input[1:]
            # print(temp_input)
            lst_output.extend(yhat.tolist())
            i = i + 1
        else:
            x_input = x_input.reshape((1, n_steps, 1))
            yhat = model.predict(x_input, verbose=0)
            logger.warn(yhat[0])
            temp_input.extend(yhat[0].tolist())
            logger.warn(len(temp_input))
            lst_output.extend(yhat.tolist())
            i = i + 1

    logger.warn("LST OUTPUT")
    logger.warn(lst_output)
    predicted_stock_price = model.predict(X_test)
    predicted_stock_price = sc.inverse_transform(predicted_stock_price)

    logger.warn("REALSTOCKPRICE")
    logger.warn(real_stock_price)
    logger.warn(len(real_stock_price))
    day_pred = np.arange(len(real_stock_price), len(real_stock_price) + 30)
    logger.warn("FUTUTE")
    logger.warn(sc.inverse_transform(lst_output))
    return sc.inverse_transform(lst_output)

def PredictStock(STOCK_INDEX: str, date_from: str, date_to: str):
    end_date = datetime.today().strftime('%Y-%m-%d') if not date_to else date_to
    logger.warn(end_date)
    data_df = yf.download(STOCK_INDEX, start=tenyear.strftime('%Y-%m-%d'), end=end_date)
    data_df.to_csv('/app/data/' + STOCK_INDEX + '.csv')
    ldata_df = yf.download(STOCK_INDEX, start=date_from,
                          end=end_date)
    ldata_df.to_csv('/app/last/' + STOCK_INDEX + '.csv')
    
    dataset_train = pd.read_csv('/app/data/' + STOCK_INDEX + '.csv')
    training_set = dataset_train.iloc[:, 1:2].values
    dataset_train.head()
    sc = MinMaxScaler(feature_range=(0, 1))
    training_set_scaled = sc.fit_transform(training_set)
    X_train = []
    y_train = []
    for i in range(60, len(dataset_train)):
        X_train.append(training_set_scaled[i - 60:i, 0])
        y_train.append(training_set_scaled[i, 0])
    X_train, y_train = np.array(X_train), np.array(y_train)
    logger.warn(dataset_train)
    X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
    model = tf.keras.models.load_model('models/'+ STOCK_INDEX)
    dataset_test = pd.read_csv('/app/last/' + STOCK_INDEX + '.csv')
    real_stock_price = dataset_test.iloc[:, 1:2].values
    dataset_total = pd.concat((dataset_train['Open'], dataset_test['Open']), axis=0)
    inputs = dataset_total[len(dataset_total) - len(dataset_test) - 60:].values
    inputs = inputs.reshape(-1, 1)
    inputs = sc.transform(inputs)
    X_test = []
    for i in range(60, dataset_test.shape[0] + 60):
        X_test.append(inputs[i - 60:i, 0])
    X_test = np.array(X_test)
    X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
    training_size = int(len(inputs) * 0.25)
    test_data = inputs[training_size:len(dataset_train), :1]
    x_input = test_data[abs(60-len(test_data)):].reshape(1, -1)
    x_input.shape
    temp_input = list(x_input)
    temp_input = temp_input[0].tolist()
    lst_output = []
    lst_dates = []

    for d in range(0, len(real_stock_price)):
        pdays = datetime.strptime(date_from, '%Y-%m-%d') + timedelta(days=d)
        lst_dates.append(pdays.strftime('%Y-%m-%d'))

    n_steps = x_input.shape[1]
    i = 0
    while (i < 30):
        ndays = datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=i+1)
        lst_dates.append(ndays.strftime('%Y-%m-%d'))
        if (len(temp_input) > x_input.shape[1]):
            x_input = np.array(temp_input[1:])
            x_input = x_input.reshape(1, -1)
            x_input = x_input.reshape((1, n_steps, 1))
            # print(x_input)
            yhat = model.predict(x_input, verbose=0)
            temp_input.extend(yhat[0].tolist())
            temp_input = temp_input[1:]
            # print(temp_input)
            lst_output.extend(yhat.tolist())
            i = i + 1
        else:
            x_input = x_input.reshape((1, n_steps, 1))
            yhat = model.predict(x_input, verbose=0)
            temp_input.extend(yhat[0].tolist())
            lst_output.extend(yhat.tolist())
            i = i + 1
    predicted_stock_price = model.predict(X_test)
    predicted_stock_price = sc.inverse_transform(predicted_stock_price)
    day_pred = np.arange(len(real_stock_price), len(real_stock_price) + 30)

    df3=real_stock_price.tolist()
    df3.extend(sc.inverse_transform(lst_output).tolist())

    logger.warn(lst_dates)
    logger.warn(df3)

    logger.warn(len(lst_dates))
    logger.warn(len(df3))
    allddd = np.array(df3)
    dasd  = allddd.reshape(1,len(df3)).tolist()
    logger.warn(dasd)
    logger.warn(len(dasd))
    graph = np.stack((lst_dates,dasd[0]), axis=1)
    objectgraph = []
    for i in range(0, len(lst_dates)):
        objectgraph.append({"Date": graph[i][0], "scales": int(float(graph[i][1]))})
    logger.warn(objectgraph)
    return objectgraph