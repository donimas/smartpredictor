from sqlalchemy.orm import Session
from starlette.responses import HTMLResponse, JSONResponse, Response
from fastapi import Body, Depends, FastAPI, HTTPException, File, UploadFile, Form
from fastapi import APIRouter
from fastapi import BackgroundTasks
from rest.crud import predict
from rest.ml import TrainStock, PredictStock
from rest.db.database import get_db
from rest.schema.Predict import PredictBase
from rest.schema.Index import Index
import logging

logger = logging.getLogger(__name__)

router = APIRouter()

def queue_train_model(index: str, date_from: str, date_to: str):
    TrainStock(index, date_from, date_to,)

@router.get("/api/predict/{index}", tags=["Prediction"])
async def get_prediction(index: str, date_from: str, date_to: str):
    pred = PredictStock(index, date_from, date_to)
    return JSONResponse(content=pred)

@router.post("/api/predict", tags=["Prediction"])
async def post_prediction(body: Index, background_tasks: BackgroundTasks):
    background_tasks.add_task(queue_train_model, body.index, body.date_from, body.date_to,)
    return {"message": "Index {body.index} was start {body.date_from}-{body.date_to} train model"}

@router.get("/api/predict/{index}/history", tags=["History"])
async def predict_history(index: str,db: Session = Depends(get_db)):
    pred = predict.get_predict(db, 1)
    return {"hello": pred}

@router.post("/api/predict/{index}/history", tags=["History"])
async def post_pred_history(body: PredictBase, index: str, db: Session = Depends(get_db)):
    return predict.create_predict(db, body)