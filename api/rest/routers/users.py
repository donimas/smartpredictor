from starlette.responses import HTMLResponse, JSONResponse, Response
from fastapi import Body, Depends, FastAPI, HTTPException, File, UploadFile, Form
from typing import List
import logging
import base64
from fastapi import APIRouter


logger = logging.getLogger(__name__)

router = APIRouter()

@router.get("/api/users", tags=["Users"])
async def list_users():
    userlist = [{"id":1, "name":"Aidos Kakimzhanov"}]
    return userlist

