from starlette.responses import HTMLResponse, JSONResponse, Response
from fastapi import Body, Depends, FastAPI, HTTPException, File, UploadFile, Form
import logging
import os.path
from fastapi import APIRouter
from fastapi import BackgroundTasks
from rest.crud import stock, history
from rest.schema.Stock import StockBase
from sqlalchemy.orm import Session
from rest.db.database import get_db
from rest.schema.Index import Index
from rest.worker import StockWorker
from rest.utils.sandp500 import sandp500

logger = logging.getLogger(__name__)

router = APIRouter()


@router.get("/api/stock/train", tags=["Train"])
async def train_stock(bt: BackgroundTasks = None):
    bt.add_task(StockWorker.start_train)
    return {"train": "ok"}

@router.get("/api/stock/train_progress", tags=["Train"])
async def start_stock():
    res = {}
    for index in sandp500:
        res[index] = False if not os.path.isdir("./models/"+index) else True
    return res

@router.get("/api/stock/train", tags=["Train"])
async def train_stock(bt: BackgroundTasks = None):
    bt.add_task(StockWorker.start_train)
    return {"train": "ok"}

@router.get("/api/stock/update", tags=["Prediction"])
async def stock_update(db: Session = Depends(get_db), bt: BackgroundTasks = None):
    return bt.add_task(StockWorker.start, db)


@router.get("/api/stock/{index}/history", tags=["History"])
async def get_stock_history(index: str, db: Session = Depends(get_db)):
    return history.get_by_index(db, index)

@router.get("/api/stock/{index}", tags=["Prediction"])
async def get_stock(index: str):
    return stock.get(index)


@router.get("/api/stock", tags=["Prediction"])
async def stock_list( q: str = None, db: Session = Depends(get_db)):
    return stock.list(db, q)

